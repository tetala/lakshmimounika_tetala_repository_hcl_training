package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bean.Books;

@Repository
public class BooksDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	public List<Books> getAllBooks(){
		try {
			return jdbcTemplate.query("select * from books",new bookRowMapper());
		} catch (Exception e) {
			System.out.println("in retriving books"+e);
			return null;
		}
		
		
	}
	public int storeLikedBooks(Books books,String user) {
		try {
			return jdbcTemplate.update("insert into likedbooks values(?,?,?,?,?,?)",books.getId(),books.getTitle(),books.getAuthor(),
					books.getGenre(),books.getImageurl(),user);
		} catch (Exception e) {
			System.out.println("in liked books"+e);
			return 0;
		}
	}
	public int storeReadLaterBooks(Books books,String user) {
		try {
			return jdbcTemplate.update("insert into readlaterbooks values(?,?,?,?,?,?)",books.getId(),books.getTitle(),books.getAuthor(),
					books.getGenre(),books.getImageurl(),user);
		} catch (Exception e) {
			System.out.println("in Readlater books"+e);
			return 0;
		}
	}
	public List<Books> getAllLikedBooks(String user){
		try {
			return jdbcTemplate.query("select id,title,author,genre,imageurl  from likedbooks where user=?",new likedBookRowMapper(),user);
		} catch (Exception e) {
			System.out.println("in getlikedBooks"+e);
			return null;
		}

}
	public List<Books> getAllReadLaterBooks(String user){
		try {
			return jdbcTemplate.query("select id,title,author,genre,imageurl  from readlaterbooks where user=?",new readLaterBookRowMapper(),user);
		} catch (Exception e) {
			System.out.println("in getlikedBooks"+e);
			return null;
		}

}
}
class bookRowMapper implements RowMapper<Books>{

	@Override
	public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
		Books book=new Books();
		book.setId(rs.getInt(1));
		book.setTitle(rs.getString(2));
		book.setAuthor(rs.getString(3));
		book.setGenre(rs.getString(4));
		book.setImageurl(rs.getString(5));
		return book;
	}
	
}
class likedBookRowMapper implements RowMapper<Books>{

	@Override
	public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
		Books lb=new Books();
		lb.setId(rs.getInt(1));
		lb.setTitle(rs.getString(2));
		lb.setAuthor(rs.getString(3));
		lb.setGenre(rs.getString(4));
		lb.setImageurl(rs.getString(5));
		return lb;
	}
	
}
class readLaterBookRowMapper implements RowMapper<Books>{

	@Override
	public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
		Books rl=new Books();
		rl.setId(rs.getInt(1));
		rl.setTitle(rs.getString(2));
		rl.setAuthor(rs.getString(3));
		rl.setGenre(rs.getString(4));
		rl.setImageurl(rs.getString(5));
		return rl;
	}
	
}


