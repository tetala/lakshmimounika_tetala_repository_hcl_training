package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bean.Login;
import com.dao.LoginDao;

@Service
public class LoginService {
	@Autowired
    LoginDao loginDao;
	public String storeRegistrationInfo(Login log) {
		if(loginDao.StoreRegistrationDetails(log)>0) {
			return "Sucess";
			
		}else {
			return "Failed";
		}
	}
	
	public String checkLoginInfo(String email) {
		return loginDao.checkLoginDetails(email);
		
		
	}

}
