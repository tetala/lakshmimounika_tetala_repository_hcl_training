package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Books;
import com.dao.BooksDao;

@Service
public class BooksService {
	@Autowired
	BooksDao booksDao;
	public List<Books> fetchAllBooks(){
		return booksDao.getAllBooks();
	}
	
	public String storeLikedBooksInfo(Books book,String user) {
		if(booksDao.storeLikedBooks(book, user)>0) {
			return "sucessfully Stored liked books";
		}else {
			return "failed to store";
		}
	}
	
	public String storeReadLaterBooksInfo(Books book,String user) {
		if(booksDao.storeReadLaterBooks(book, user)>0) {
			return "sucessfully Stored readlater books";
		}else {
			return "failed to store";
		}
	}
	
	public List<Books> fetchAllLikedBooks(String user){
		return booksDao.getAllLikedBooks(user);
	}
	public List<Books> fetchAllReadLikedBooks(String user){
		return booksDao.getAllReadLaterBooks(user);
	}
	
 
}
