package com.gl.hcl.week2assignment;

public class Employee {
	int id;
	String name;
	int age; 
	int salary; //per annum
	String department;
	String city;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		try {
			if(id<0) {
				throw new IllegalArgumentException("Employee id should not be less than or equal to zero");
			}
		this.id = id;
		}catch (Exception e) {
			System.out.println(" "+e.getMessage());
		}
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		try {
			if(name.isEmpty()||name==null) {
				throw new IllegalArgumentException("Employee name should not be empty or null");
			}
				
		this.name = name;
		}catch (Exception e) {
			System.out.println(" "+e.getMessage());
		}
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		try {
			if(age<0) {
				throw new IllegalArgumentException("Employee age should not be less than or equal to zero");
			}
		this.age = age;
		}catch (Exception e) {
			System.out.println(" "+e.getMessage());
		}
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		try {
			if(salary<0) {
				throw new IllegalArgumentException("Employee salary should not be less than or equal to zero");
			}
		this.salary = salary;
		}catch (Exception e) {
			System.out.println(" "+e.getMessage());
		}
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		if(department.isEmpty()||department==null) {
			throw new IllegalArgumentException("departemnt should not be empty or null");
		}
		this.department = department;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		try {
			if(city.isEmpty()||city==null) {
				throw new IllegalArgumentException("city should not be empty or null");
			}
		this.city = city;
		}catch (Exception e) {
			System.out.println(" "+e.getMessage());
		}
	}
	public void getEmployeeDetails() {
		System.out.println(" "+id+" "+name+" "+salary+" "+department+" "+city);
		
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", age=" + age + ", salary=" + salary + ", department="
				+ department + ", city=" + city + "]";
	}
	
	
	

}
