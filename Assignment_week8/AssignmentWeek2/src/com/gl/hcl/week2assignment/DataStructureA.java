package com.gl.hcl.week2assignment;

import java.util.ArrayList;
import java.util.Collections;

public class DataStructureA {
	public void sortingNames(ArrayList<Employee> employees) {
		ArrayList<String> name = new ArrayList<String>();
		name.add(employees.get(0).getName());
		name.add(employees.get(1).getName());
		name.add(employees.get(2).getName());
		name.add(employees.get(3).getName());
		name.add(employees.get(4).getName());
		System.out.println("-----------------------------------------");
		System.out.println("Names Of All Employees in The Sorted Order Are ");

		Collections.sort(name);
		System.out.println("" + name);

	}

}
