package com.gl.hcl.week2assignment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class DataStructureB {
	public void cityNameCount(ArrayList<Employee> employees) {
		Set<String> c = new TreeSet<String>();
		ArrayList<String> nc = new ArrayList<String>();
		for (Employee s : employees) {
			nc.add(s.getCity());
			c.add(s.getCity());
		}
		System.out.println("------------------------------------------");
		System.out.println("count of employees from each city");
		for (String s : c) {
			
			System.out.print(s + "=" + Collections.frequency(nc, s) + " ");
		}
		System.out.println("\n");

	}

	public void monthlySalary(ArrayList<Employee> employees) {
		System.out.println("--------------------------------------------");
		System.out.println("Monthly Salary Of Employees Along With Their Id");
		for (Employee e : employees) {
			
			System.out.print(e.getId() + "=" + e.getSalary() / 12 +" ");
		
		}

	}

}
