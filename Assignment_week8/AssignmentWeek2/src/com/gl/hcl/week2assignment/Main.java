package com.gl.hcl.week2assignment;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		ArrayList<Employee> employee = new ArrayList<>();
		// creation of employee objects and setting values
		Employee e1 = new Employee();
		e1.setId(1);
		e1.setName("Aman");
		e1.setAge(20);
		e1.setSalary(11000000);
		e1.setDepartment("IT");
		e1.setCity("Delhi");
		
		Employee e2 = new Employee();
		e2.setId(2);
		e2.setName("Bobby");
		e2.setAge(22);
		e2.setSalary(500000);
		e2.setDepartment("HR");
		e2.setCity("Bombay");

		Employee e3 = new Employee();
		e3.setId(3);
		e3.setName("Zoe");
		e3.setAge(20);
		e3.setSalary(750000);
		e3.setDepartment("Admin");
		e3.setCity("Delhi");
		Employee e4 = new Employee();
		e4.setId(4);
		e4.setName("Smitha");
		e4.setAge(21);
		e4.setSalary(1000000);
		e4.setDepartment("IT");
		e4.setCity("Chennai");

		Employee e5 = new Employee();
		e5.setId(5);
		e5.setName("Smitha");
		e5.setAge(24);
		e5.setSalary(1200000);
		e5.setDepartment("HR");
		e5.setCity("Bengaluru");

		// Adding employee details to ArrayList
		employee.add(e1);
		employee.add(e2);
		employee.add(e3);
		employee.add(e4);
		employee.add(e5);

		// getting employee details
		e1.getEmployeeDetails();
		e2.getEmployeeDetails();
		e3.getEmployeeDetails();
		e4.getEmployeeDetails();
		e5.getEmployeeDetails();
		
		
		//Sorting employee names
		DataStructureA sortname=new DataStructureA();
		sortname.sortingNames(employee);
		
		//Counting of City names
		DataStructureB countcity=new DataStructureB();
		countcity.cityNameCount(employee);
		
		//monthly salary
		DataStructureB monthlysalary=new DataStructureB();
		monthlysalary.monthlySalary(employee);

	}

}
