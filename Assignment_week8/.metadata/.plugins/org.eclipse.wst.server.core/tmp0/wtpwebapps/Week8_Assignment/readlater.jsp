<%@page import="com.bean.Books"%>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
body {
	background-image: url("https://media.istockphoto.com/vectors/01open-book-and-creative-paper-airplanes-teamwork-paper-art-style-vector-id1189849703?k=20&m=1189849703&s=612x612&w=0&h=ViTOSts22Be3PJY0HD_2dLSF31VE5BgD0Sm7ZB96DQ8=")
	}
</style>
</head>
<h4 align="center">User Your ReadLater BookList is here</h4>
<a href="Welcome.jsp">Click Here To Go Back To Dashboard</a>
<body style="background-color:yellow; ;">
<div align="center">
<table border="1">
	<tr bgcolor="red">
			<th>Id</th>
			<th>TITLE</th>
			<th>AUTHOR</th>
			<th>GENRE</th>
			<th>Image</th>
			
	</tr>
<% 

	Object user=session.getAttribute("user");
	if(user!=null){
	out.println("WELCOME TO ReadLaterSection Ms/Mr  "+user);
	}
	Object obj = session.getAttribute("obj3");
	List<Books> listOfBooks = (List<Books>)obj;
	Iterator<Books> ii = listOfBooks.iterator();
	while(ii.hasNext()){
		Books book  = ii.next();
		%>
		<tr>
			<td><%=book.getId() %></td>
			<td><%=book.getTitle() %></td>
			<td><%=book.getAuthor() %></td>
			<td><%=book.getGenre() %></td>
			<td><img src="<%=book.getImageurl()%>"height="200px" width="150px"></td>
			
		</tr>
		<% 
	}
%>
</table>
</div>
</body>
</html>