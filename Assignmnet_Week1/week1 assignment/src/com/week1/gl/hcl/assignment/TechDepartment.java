package com.week1.gl.hcl.assignment;

public class TechDepartment extends SuperDepatment  {
	String depName() {
		return"Tech Department";
	}
	String work() {
		return "Complete coding of module 1";
	}
	String workDeadline() {
		return "Complete by EOD";
	}
	String techStackInfo() {
		return"core java";
	}

}
