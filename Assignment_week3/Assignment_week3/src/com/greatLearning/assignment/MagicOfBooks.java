package com.greatLearning.assignment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class MagicOfBooks {
	HashMap<Integer,Book>bookMap=new HashMap<>();
	TreeMap<Double,Book>bookTree=new TreeMap<>();
	ArrayList<Book>listofbooks=new ArrayList<>();
	Scanner sc=new Scanner(System.in);
	
	public void addBook() {
		Book b=new Book();
		
		System.out.println("Enter the BookId");
		b.setId(sc.nextInt());
		
		System.out.println("Enter the name of the Book");
		b.setName(sc.next());
		
		System.out.println("Enter the price of the book");
		b.setPrice(sc.nextDouble());
		
		System.out.println("enter the book genre");
		b.setGenre(sc.next());
		
		System.out.println("enter noOfBooks Sold");
		b.setNoOfCopiesSold(sc.nextInt());
		
		System.out.println("enter a book status");
		b.setBookStatus(sc.next());
		
		System.out.println("check your book added or not");
		bookMap.put(b.getId(), b);
		System.out.println("Book Added Sucessfully");
		
		bookTree.put(b.getPrice(), b);
		listofbooks.add(b);		
	}
	public void deleteBook() {
		System.out.println("Enter the bookId");
		int id=sc.nextInt();
		bookMap.remove(id);
		System.out.println("book deleted sucessfully");
	}
 	public void updateBook() {

		Book b=new Book();
		
		System.out.println("Enter the BookId");
		b.setId(sc.nextInt());
		
		System.out.println("Enter the name of the Book");
		b.setName(sc.next());
		
		System.out.println("Enter the price of the book");
		b.setPrice(sc.nextDouble());
		
		System.out.println("enter the book genre");
		b.setGenre(sc.nextLine());
		
		System.out.println("enter noOfBooks Sold");
		b.setNoOfCopiesSold(sc.nextInt());
		
		System.out.println("enter a book status");
		b.setBookStatus(sc.next());
		
		bookMap.put(b.getId(), b);
		System.out.println("Book Updated Sucessfully");
 		
 	}
 	public void displayBookInformation() {
 		if(bookMap.size()>0) {
 			Set<Integer>keySet=bookMap.keySet();
 			for(Integer key:keySet) {
 				System.out.println(key+" "+bookMap.keySet());
 			}
 				
 		}else {
 			System.out.println("temporarly unavailable");
 		}
 	}
 	public void bookCount() {
 		System.out.println("NoOf books avaliable are"+bookMap.size());
 	}
	public void autoBiography() {
		String bestSelling="AutoBiography";
		ArrayList<Book>genreBooks=new ArrayList<>();
		Iterator<Book> itr=listofbooks.iterator();
		while(itr.hasNext()) {
			Book b1=(Book)itr.next();
			if(b1.getGenre().equals(bestSelling)) {
				genreBooks.add(b1);
				System.out.println(genreBooks);
				
				
			}
		}
		
	}
	public void byFeature(int flag) {
		if(flag==1)
		{
			if (bookTree.size() > 0) 
				{
				  Set<Double> keySet = bookTree.keySet();
	   			  for (Double key : keySet) {
					System.out.println(key + " ----> " +bookTree.get(key));
	   			  }
			} else {
						System.out.println("treeMap is Empty");
					}
		}
		if(flag==2) 
		{
			 Map<Double, Object> treeMapReverseOrder= new TreeMap<>(bookTree);
			  NavigableMap<Double, Object> nmap = ((TreeMap<Double, Object>) treeMapReverseOrder).descendingMap();
			      
			  System.out.println("Book Details:");
			      
			   if (nmap.size() > 0) 
			     {
					Set<Double> keySet = nmap.keySet();
						for (Double key : keySet) {
								System.out.println(key + " ----> " + nmap.get(key));
							}
					}else{
						System.out.println("treeMap is Empty");
					}
			
		 }
		
		if(flag==3) 
		{
		    String bestSelling = "B";
	        ArrayList<Book> genreBooks = new ArrayList<Book>();
		         
		         Iterator<Book> iter=genreBooks.iterator();
		         
		         while(iter.hasNext())
		         {
		             Book b=(Book)iter.next();
		             if(b.getBookStatus().equals(bestSelling)) 
		             {
		            	 genreBooks.add(b);
		            	
		             }
			         
	              }
		         System.out.println(genreBooks);
		         
		   }
	    	

	}
		
	
	
}
