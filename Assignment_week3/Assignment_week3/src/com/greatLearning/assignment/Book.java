package com.greatLearning.assignment;

public class Book {
	private int id;
	private String name;
	private double price;
	private String Genre;
	private int noOfCopiesSold;
	private String bookStatus;
	
	

	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Book(int id, String name, double price, String genre, int noOfCopiesSold, String bookStatus) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		Genre = genre;
		this.noOfCopiesSold = noOfCopiesSold;
		this.bookStatus = bookStatus;
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getGenre() {
		return Genre;
	}

	public void setGenre(String genre) {
		Genre = genre;
	}

	public int getNoOfCopiesSold() {
		return noOfCopiesSold;
	}

	public void setNoOfCopiesSold(int noOfCopiesSold) {
		this.noOfCopiesSold = noOfCopiesSold;
	}

	public String getBookStatus() {
		return bookStatus;
	}

	public void setBookStatus(String bookStatus) {
		this.bookStatus = bookStatus;
	}



	@Override
	public String toString() {
		return "Book [id=" + id + ", name=" + name + ", price=" + price + ", Genre=" + Genre + ", noOfCopiesSold="
				+ noOfCopiesSold + ", bookStatus=" + bookStatus + "]";
	}

	

}
