package com.greatLearning.assignment;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		MagicOfBooks MOB =new MagicOfBooks();
		String in;
		do {
		System.out.println("Enter your choice 1.Adding a book, 2.deleting a Book"
				+ "3.updating a book, 4.display all the books, 5.to know total noof books available"
				+ "6.To search for autobiography books,  7.To display by all features");
        int ch=sc.nextInt();
        switch(ch) { 
                      
        case 1:      System.out.println("enter the noof books you want to store");
                     int n=sc.nextInt();
                     for(int i=0;i<n;i++) {
                    	 MOB.addBook();
                     }
                     break;
        case 2:      MOB.deleteBook();
                     break;
        case 3:      MOB.updateBook();
                     break;
        case 4:      MOB.displayBookInformation();
                     break;
        case 5:      MOB.bookCount();
                     break;
        case 6:      MOB.autoBiography();
                     break;
        case 7:      System.out.println("enter type of sorting\n 1:price low to high\n"
        		     + "2.price high to low\n"+"3.best selling");
                     int sch=sc.nextInt();
                     switch(sch) {
                     case 1: MOB.byFeature(1);
                             break;
                     case 2: MOB.byFeature(2);
                             break;
                     case 3: MOB.byFeature(3);        
                             break;
                     }
        default :    System.out.println("Wrong Choice");             
                    
        }
        System.out.println("do you want to continue");
        in=sc.next();
		}while(in.equalsIgnoreCase("Yes"));
        
	}

}
