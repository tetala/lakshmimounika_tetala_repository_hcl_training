package com.controller;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.bean.Items;
import com.bean.Users;
import com.service.ItemsService;

@Controller
public class ItemsController {
	@Autowired
	ItemsService itemsService;
	
	List<Items>listOfItems;
	
	@GetMapping(value="MenuStore")
	public String storeMenu() {
		return "menuStore";
	}
	@PostMapping(value="storeMenu")
	public String storeInfoOfMenu(HttpServletRequest req,HttpSession hs) {
		int itemId=Integer.parseInt(req.getParameter("itemId"));
		String itemName=req.getParameter("itemName");
		String itemType=req.getParameter("itemType");
		float itemPrice=Float.parseFloat(req.getParameter("itemPrice"));
		Items item=new Items();
		item.setItemId(itemId);
		item.setItemName(itemName);
		item.setItemType(itemType);
		item.setItemPrice(itemPrice);
	    String res=itemsService.storeMenuItem(item);
	    hs.setAttribute("res2", res);	    
		return "messagepage";				
	}
	
	@GetMapping(value = "allDetails")
	public String getAllItemsInfoForUser(HttpSession hs) {
		listOfItems=itemsService.getAllItems();
		hs.setAttribute("items", listOfItems);		
		return "adminMenu";		
	}
	
	@GetMapping(value="deleteItem")
	public String deleteItem(HttpServletRequest req,HttpSession hs) {
		int itemId=Integer.parseInt(req.getParameter("itemId"));
	    String result=itemsService.deleteItemInfo(itemId);
		hs.setAttribute("result", result);
		listOfItems=itemsService.getAllItems();
		hs.setAttribute("items", listOfItems);		
		return "adminMenu";				
	}
	
	@GetMapping(value="UpdateItem")
	public String updateItem(HttpServletRequest req,HttpSession hs) {
		int itemId=Integer.parseInt(req.getParameter("itemId"));
		hs.setAttribute("itemId", itemId);
		return "updateItem";				
	}
	
	@PostMapping(value="updateItemInfo")
	public String updateItemInfo(HttpServletRequest req,HttpSession hs) {
		int itemId=Integer.parseInt(req.getParameter("itemId"));
		String itemName=req.getParameter("itemName");
		String itemType=req.getParameter("itemType");
		float itemPrice=Float.parseFloat(req.getParameter("itemPrice"));
		
		Items item=new Items();
		item.setItemId(itemId);
		item.setItemName(itemName);
		item.setItemType(itemType);
		item.setItemPrice(itemPrice);
		
	    String result=itemsService.updateItemPrice(item);
		hs.setAttribute("result", result);
		listOfItems=itemsService.getAllItems();
		hs.setAttribute("items", listOfItems);		
		return "adminMenu";				
	}
	
	@GetMapping(value = "all")
	public String getAllItemsInfo(HttpSession hs) {
		listOfItems=itemsService.getAllItems();
		hs.setAttribute("items", listOfItems);		
		return "menu";	
	}

	
	
	

}
