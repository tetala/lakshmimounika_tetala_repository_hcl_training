create database lakshmi_mounika_hcl;

use lakshmi_mounika_hcl;

create table Books (id int primary key,title varchar(50),author varchar(50),genre varchar(20),imageurl varchar(512));

insert into Books values(1,"life_without_limits","Nick_Yujicik","Narrative","https://images-eu.ssl-images-amazon.com/images/I/51utIqP-JkL._SY264_BO1,204,203,200_QL40_FMwebp_.jpg");
insert into Books values(2,"Born_Again_Mountain","Arunima_Sinha","Novel","https://images-eu.ssl-images-amazon.com/images/I/41hl9dp25oL._SY264_BO1,204,203,200_QL40_FMwebp_.jpg");
insert into Books values(3,"TheEverydayHero","Robin_Sharma","Fantasy","https://images-eu.ssl-images-amazon.com/images/I/41txBobui9S._SY264_BO1,204,203,200_QL40_FMwebp_.jpg");
insert into Books values(4,"The_Curmudgeon's_Guide_to_Getting_Ahead","Charles_Murray","comic","https://images-eu.ssl-images-amazon.com/images/I/41A0B1kUthL._SY264_BO1,204,203,200_QL40_FMwebp_.jpg");
insert into Books values(5,"Learning_how_to_fly","A.P.J._Abdul_Kalam","Inspirational","https://images-eu.ssl-images-amazon.com/images/I/415xfYGetCL._SY264_BO1,204,203,200_QL40_FMwebp_.jpg");
insert into Books values(6,"Journey","A.P.J._Abdul_Kalam","Classics","https://images-na.ssl-images-amazon.com/images/I/41+h0fYL-eL._SY344_BO1,204,203,200_.jpg");
insert into Books values(7,"Ignited_minds","A.P.J._Abdul_Kalam","NonFiction","https://images-eu.ssl-images-amazon.com/images/I/41uV5nrOfsL._SY264_BO1,204,203,200_QL40_FMwebp_.jpg");
insert into Books values(8,"Wings_of_fire","A.P.J._Abdul_Kalam","AutoBiography","https://images-eu.ssl-images-amazon.com/images/I/51vgy3LMz6L._SY264_BO1,204,203,200_QL40_FMwebp_.jpg");
insert into Books values(9,"Swami_Vivekananda","Swami_Vivekananda","Biography","https://images-eu.ssl-images-amazon.com/images/I/41raS4oJssL._SY264_BO1,204,203,200_QL40_FMwebp_.jpg");
insert into Books values(10,"A_biography","Mamta_Sharma","Biography","https://images-eu.ssl-images-amazon.com/images/I/51ac9W8HqCL._SX198_BO1,204,203,200_QL40_FMwebp_.jpg");
insert into Books values(11,"Alexander_the_Great","Jacob_Abbott","Biography","https://images-na.ssl-images-amazon.com/images/I/51DVQNCm0+S._SX258_BO1,204,203,200_.jpg");

create table Login(email varchar(20) primary key,password varchar(20) not null);

create table LikedBooks(id int,title varchar(50),author varchar(50),genre varchar(20),imageurl varchar(512),user varchar(20));

create table ReadLaterBooks(id int,title varchar(50),author varchar(50),genre varchar(20),imageurl varchar(512),user varchar(20));