package com.books.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.books.bean.Books;
import com.books.resource.DbResource;

public class BooksDao {
	public List<Books> getAllBooks(){
		List<Books>listOfBooks=new ArrayList<>();
		try {
			Connection con=DbResource.getConnection();
			PreparedStatement pst=con.prepareStatement("select * from books");
			ResultSet rs=pst.executeQuery();
			while(rs.next()) {
				Books book=new Books();
				book.setId(rs.getInt(1));
				book.setTitle(rs.getString(2));
				book.setAuthor(rs.getString(3));
				book.setGenre(rs.getString(4));
				book.setImageurl(rs.getString(5));
				listOfBooks.add(book);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return listOfBooks;
	}
		
	public int storeLikedBooks(Books book,String user) {	
		try {
			Connection con=DbResource.getConnection();
			PreparedStatement pst=con.prepareStatement("insert into likedbooks values(?,?,?,?,?,?)");
			pst.setInt(1,book.getId());
			pst.setString(2,book.getTitle());
			pst.setString(3,book.getAuthor());
			pst.setString(4,book.getGenre());
			pst.setString(5,book.getImageurl());
			pst.setString(6, user);
	        return pst.executeUpdate();			
		} catch (Exception e) {
			System.out.println("In Liked Books");
			return 0;
		}
		
	}
	public List<Books> getLikedBooks(String user) {
		List<Books>listOfBooks=new ArrayList<>();
		try {
			Connection con=DbResource.getConnection();
			PreparedStatement pst=con.prepareStatement("select id,title,author,genre,imageurl from likedbooks where user=?");
			pst.setString(1, user);
			ResultSet rs=pst.executeQuery();
			while(rs.next()) {
				Books book=new Books();
				book.setId(rs.getInt(1));
				book.setTitle(rs.getString(2));
				book.setAuthor(rs.getString(3));
				book.setGenre(rs.getString(4));
				book.setImageurl(rs.getString(5));
				listOfBooks.add(book);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return listOfBooks;
	}
		
	public int storeReadLaterBooks(Books book,String user) {	
		try {
			Connection con=DbResource.getConnection();
			PreparedStatement pst=con.prepareStatement("insert into readlater values(?,?,?,?,?,?)");
			pst.setInt(1,book.getId());
			pst.setString(2,book.getTitle());
			pst.setString(3,book.getAuthor());
			pst.setString(4,book.getGenre());
			pst.setString(5,book.getImageurl());
			pst.setString(6, user);
	        return pst.executeUpdate();			
		} catch (Exception e) {
			System.out.println("In readlater Books"+e);
			return 0;
		}
		
	}
	public List<Books> getReadLaterBooks(String user) {
		List<Books>listOfBooks=new ArrayList<>();
		try {
			Connection con=DbResource.getConnection();
			PreparedStatement pst=con.prepareStatement("select id,title,author,genre,imageurl from readlater where user=?");
			pst.setString(1, user);
			ResultSet rs=pst.executeQuery();
			while(rs.next()) {
				Books book=new Books();
				book.setId(rs.getInt(1));
				book.setTitle(rs.getString(2));
				book.setAuthor(rs.getString(3));
				book.setGenre(rs.getString(4));
				book.setImageurl(rs.getString(5));
				listOfBooks.add(book);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return listOfBooks;
	}

}
