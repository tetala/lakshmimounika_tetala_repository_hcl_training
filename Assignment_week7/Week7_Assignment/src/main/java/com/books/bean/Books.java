package com.books.bean;

public class Books {
	private int id;
	private String title;
	private String author;
	private String genre;
	private String imageurl;
	
	public Books() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Books(int id, String title, String author, String genre, String imageurl) {
		super();
		this.id = id;
		this.title = title;
		this.author = author;
		this.genre = genre;
		this.imageurl = imageurl;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getImageurl() {
		return imageurl;
	}
	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	@Override
	public String toString() {
		return "Books [id=" + id + ", title=" + title + ", author=" + author + ", genre=" + genre + ", imageurl="
				+ imageurl + "]";
	}
	

}
