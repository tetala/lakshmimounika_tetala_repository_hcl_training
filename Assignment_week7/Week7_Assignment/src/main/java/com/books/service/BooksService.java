package com.books.service;

import java.util.List;

import com.books.bean.Books;
import com.books.dao.BooksDao;

public class BooksService {
	public List<Books> getAllBooksInfo() {
		BooksDao bd=new BooksDao();
		return bd.getAllBooks();		
	}
	
	public String storeLikedBooksInfo(Books book,String user) {
		BooksDao bd=new BooksDao();
		if(bd.storeLikedBooks(book,user)>0) {
			return "Sucessfully Stored";
		}else {
			return null;
		}
	}
	public List<Books> getLikedBooksInfo(String user) {
		BooksDao bd=new BooksDao();
		return bd.getLikedBooks(user);		
	}
	
	public String storeReadLaterBooksInfo(Books book,String user) {
		BooksDao bd=new BooksDao();
		if(bd.storeReadLaterBooks(book,user)>0) {
			return "Sucessfully Stored";
		}else {
			return null;
		}
	}
	public List<Books> getReadLaterBooksInfo(String user) {
		BooksDao bd=new BooksDao();
		return bd.getReadLaterBooks(user);		
	}

}
