package com.books.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.books.bean.Books;
import com.books.service.BooksService;

/**
 * Servlet implementation class BooksController
 */
public class BooksController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	List<Books> listOfBooks ; 
	HttpSession hs ;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BooksController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		BooksService es = new BooksService();
		listOfBooks= es.getAllBooksInfo();	
		hs= request.getSession();
		hs.setAttribute("obj1",listOfBooks);
		RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
		rd.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		PrintWriter pw=response.getWriter();
		
		if(request.getParameter("Like")!=null) {
			int id=Integer.parseInt(request.getParameter("id"));
			pw.println(id);
			Iterator<Books>ii=listOfBooks.iterator();
			while(ii.hasNext()) {
				Books books=ii.next();
				BooksService es=new BooksService();
				if(id==books.getId()) {
					Object user=hs.getAttribute("obj");
					String result=es.storeLikedBooksInfo(books,user.toString());
					RequestDispatcher rd2=request.getRequestDispatcher("index.jsp");
					rd2.include(request, response);
				}
			}
			
		}
		//tocheck readlater form is submitted or not
		if(request.getParameter("readlater")!=null) {
			
			int id=Integer.parseInt(request.getParameter("id"));
			pw.println(id);
			Iterator<Books>ii=listOfBooks.iterator();
			while(ii.hasNext()) {
				Books books=ii.next();
				BooksService es=new BooksService();
				if(id==books.getId()) {
					Object user=hs.getAttribute("obj");
					String res=es.storeReadLaterBooksInfo(books,user.toString());
					RequestDispatcher rd2=request.getRequestDispatcher("index.jsp");
					rd2.include(request, response);
				}
			}
			
		}
		
	}
	

}
