package com.login.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.login.bean.Login;
import com.login.service.LoginService;

/**
 * Servlet implementation class LoginController
 */
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter pw=response.getWriter();
		String email=request.getParameter("email");
		String pass=request.getParameter("pass");
		Login login=new Login();
		login.setEmail(email);
		login.setPassword(pass);
		LoginService ls=new LoginService();
		String res=ls.checkLoginDetails(login);	
		RequestDispatcher rd1=request.getRequestDispatcher("DashBoard.jsp");
        RequestDispatcher rd2=request.getRequestDispatcher("index.jsp");
		if(res.equalsIgnoreCase("sucess")) {			
            rd1.forward(request, response);
            String user=email.substring(0,email.indexOf("@"));
		    HttpSession hs=request.getSession();
            hs.setAttribute("obj",user);
          
		}else {
			pw.println("<font color='red'>PLEASE ENTER VALID DETAILS OR REGISTER</font><br>");
			response.setContentType("text/html");
			rd2.include(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		PrintWriter pw=response.getWriter();
		response.setContentType("text/html");
		String email=request.getParameter("email");
		String pass=request.getParameter("pass");
		Login login=new Login();
		login.setEmail(email);
		login.setPassword(pass);
		LoginService ls=new LoginService();
		String res=ls.storeRegistrationDetais(login);
		RequestDispatcher rd1=request.getRequestDispatcher("Login.jsp");
		RequestDispatcher rd2=request.getRequestDispatcher("index.jsp");
		if(res.equalsIgnoreCase("sucess")) {
			pw.println("<h4 align='center'>Sucessfully Registered</h4><br>");
			response.setContentType("text/html");
			rd1.include(request, response);		
		}else {
			pw.println("<h3 align='center'>User Your Details Are Not Registerd! Try Again </h3><br>");
			response.setContentType("text/html"); 
			rd2.forward(request, response);
		}
	}

}
