package com.login.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.books.resource.DbResource;
import com.login.bean.Login;

public class LoginDao {
	public int StoreRegistrationInfo(Login login) {
		try {
			Connection con=DbResource.getConnection();
			PreparedStatement pst=con.prepareStatement("insert into login values(?,?)");
			pst.setString(1,login.getEmail());
			pst.setString(2,login.getPassword());
			return pst.executeUpdate();
		} catch (SQLException e) {
			System.out.println("in registration"+e);
			return 0;
		}		
	}
	public int checkLoginInfo(Login login) {
		try {
			Connection con=DbResource.getConnection();
			PreparedStatement pst=con.prepareStatement("select * from login where email=? and password=?");
//			PreparedStatement pst=con.prepareStatement("select password from login where email=?");
			pst.setString(1,login.getEmail());
			pst.setString(2,login.getPassword());
			ResultSet rs=pst.executeQuery();
			if(rs.next()) {
				String email=rs.getString(1);
				String pass=rs.getString(2);
				if(login.getEmail().equals(email)&&login.getPassword().equals(pass)) {
					return 1;
				}
			
			}
		} catch (Exception e) {
			System.out.println("in checklogin"+e);
			return 0;
		}
		return 0;
		
		
	}

}
