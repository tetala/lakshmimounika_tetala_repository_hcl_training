<%@page import="com.books.service.BooksService"%>
<%@page import="com.books.bean.Books"%>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Liked page</h2>
<div align="center">
<table border="1">
	<tr>
			<th>Id</th>
			<th>TITLE</th>
			<th>AUTHOR</th>
			<th>GENRE</th>
			<th>IMAGE</th>
			
	</tr>
<%  
    Object obj=session.getAttribute("obj");
    out.println("WELCOME USER "+obj);
    BooksService bs=new BooksService();
	List<Books> listOfBooks = bs.getReadLaterBooksInfo(obj.toString());
	Iterator<Books> ii = listOfBooks.iterator();
	while(ii.hasNext()){
		Books book  = ii.next();
		%>
		<tr>
			<td><%=book.getId() %></td>
			<td><%=book.getTitle() %></td>
			<td><%=book.getAuthor() %></td>
			<td><%=book.getGenre() %></td>
			<td><img src="<%=book.getImageurl()%>"></td>
		</tr>
		<% 
	}
%>
</table>
</div>
</body>
</html>