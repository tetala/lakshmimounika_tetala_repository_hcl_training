<%@page import="com.books.bean.Books"%>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<a href="Login.jsp">Click_Here_TO_Login</a><br>
<h2 align="center"> WELCOME TO BOOKESS STORE</h2>
<div align="center">>
<table border="1">
	<tr>
			<th>Id</th>
			<th>TITLE</th>
			<th>AUTHOR</th>
			<th>GENRE</th>
			<th>Image</th>
			
	</tr>
<%  //Run BOOKSController for better results
   response.sendRedirect("BooksController");
	Object obj1 = session.getAttribute("obj1");
	List<Books> listOfBooks = (List<Books>)obj1;
	Iterator<Books> ii = listOfBooks.iterator();
	while(ii.hasNext()){
		Books book  = ii.next();
		%>
		<tr>
			<td><%=book.getId() %></td>
			<td><%=book.getTitle() %></td>
			<td><%=book.getAuthor() %></td>
			<td><%=book.getGenre() %></td>
			<td><img src="<%=book.getImageurl()%>"></td>
			
		</tr>
		<% 
	}
%>
</table>
</div>

</body>
</html>