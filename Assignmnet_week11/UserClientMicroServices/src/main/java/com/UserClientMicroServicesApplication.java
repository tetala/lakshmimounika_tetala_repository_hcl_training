package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication(scanBasePackages = "com")
@EnableEurekaClient
public class UserClientMicroServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserClientMicroServicesApplication.class, args);
		System.err.println("Userclient running on port 8289");
	}

}
