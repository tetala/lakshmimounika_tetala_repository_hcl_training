package com.bean.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.user.bean.Users;
import com.user.service.UserService;

@RestController
@RequestMapping(value="user")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@PostMapping(value="register",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String registerUser(@RequestBody Users us) {
		return userService.storeUserDetails(us);
	}
	
	@PostMapping(value="login")
	public String loginUser(@RequestBody Users Us) {
		return userService.checkUserDetails(Us);
	}
	
	@PostMapping(value="logout")
	public String logoutUser(@RequestBody Users us ) {
		return us.getUsername()+" "+"logged out sucessfully";
	}

}
