package com.user.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.user.bean.Users;

@Repository
public interface UserDao extends JpaRepository<Users, String>{

}
