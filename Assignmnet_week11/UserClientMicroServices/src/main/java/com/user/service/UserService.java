package com.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.user.bean.Users;
import com.user.dao.UserDao;

@Service
public class UserService {
	
	@Autowired
	UserDao userDao;
	
	public String storeUserDetails(Users user) {
		if(userDao.existsById(user.getEmail())&& userDao.existsById(user.getUsername())&&userDao.existsById(user.getPassword())) {
			return user.getUsername()+" "+"already exists!..";
		}else {
			userDao.save(user);
			return user.getUsername()+" "+"registered sucessfully";
		}
	}
		
		public String checkUserDetails(Users user) {
			if(userDao.existsById(user.getEmail())) {
				Users u=userDao.getById(user.getEmail());
				if(u.getUsername().equalsIgnoreCase(user.getUsername())&&
						u.getEmail().equalsIgnoreCase(user.getEmail())&&
						u.getPassword().equalsIgnoreCase(user.getPassword())) {
				return user.getUsername()+" "+"Sucessfully Logged In!";
				
			}else {
				return user.getUsername()+" "+"enter your correct username or password";
			}
			}else {
				return user.getEmail()+"Is Incorrect";
			}
		}
}
