package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class Week11AssignmentServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week11AssignmentServerApplication.class, args);
	    System.err.println("Eureka Server Running On Port 8761");
	}

}
