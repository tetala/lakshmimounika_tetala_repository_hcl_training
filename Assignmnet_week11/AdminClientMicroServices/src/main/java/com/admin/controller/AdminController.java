package com.admin.controller;

import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.admin.bean.Admin;
import com.admin.service.AdminService;

@RestController
@RequestMapping(value="admin")
public class AdminController {
	
	@Autowired
	AdminService adminService;
	
	@PostMapping(value="register", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String registerAdmin(@RequestBody Admin admin) {
		return  adminService.storeAdminDetails(admin);	
	}
	
	@PostMapping(value="login")
	public String loginAdmin(@RequestBody Admin admin) {
		return adminService.checkAdminLogin(admin);		
	}
	
	@PostMapping(value="logout")
	public String logoutAdmin(@RequestBody Admin admin) {
		return admin.getUsername()+" " +"logged out sucessfully";
	}

}
