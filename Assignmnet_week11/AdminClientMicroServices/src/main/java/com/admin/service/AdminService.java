package com.admin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.admin.bean.Admin;
import com.admin.dao.AdminDao;

@Service
public class AdminService {
	
	@Autowired
	AdminDao adminDao;
	
	public String storeAdminDetails(Admin admin) {
		if(adminDao.existsById(admin.getEmail()) && adminDao.existsById(admin.getPassword())) {
			return "Your Details Already Present";
		}else {
			adminDao.save(admin);
			return admin.getUsername()+" "+"Your Details Are Saved Sucessfully";	
		}
	}
	
	public String checkAdminLogin(Admin admin) {
		if(adminDao.existsById(admin.getEmail())) {
		Admin ad=adminDao.getById(admin.getEmail());
		if(ad.getUsername().equalsIgnoreCase(admin.getUsername()) && 
				ad.getEmail().equalsIgnoreCase(admin.getEmail()) 
				&& ad.getPassword().equalsIgnoreCase(admin.getPassword())) {
			return admin.getUsername() +" "+ "logged in sucessfully";
		}else {
			return admin.getUsername()+" "+"Details not present";
		}
		}else {
			return admin.getUsername()+" "+"does not exist check Your Details!";
		}
		
	}
	

}
