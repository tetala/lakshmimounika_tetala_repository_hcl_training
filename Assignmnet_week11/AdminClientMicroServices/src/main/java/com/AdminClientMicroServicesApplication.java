package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = "com")
@EnableEurekaClient
@EnableJpaRepositories(basePackages = "com.admin.dao")
@EntityScan(basePackages = "com")
@EnableSwagger2
public class AdminClientMicroServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminClientMicroServicesApplication.class, args);
		System.err.println("Adminclient running on port 8389");
	}
	
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).
				select().apis(RequestHandlerSelectors.
						basePackage("com")).build();
	}

}
