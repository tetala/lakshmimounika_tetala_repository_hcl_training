package com.greatlearning.miniproject;

import java.util.Date;
import java.util.List;

public class Bills {
	private String name;
	private List<Items>items;
	private float cost;
	private Date time;
	public Bills() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Bills(String name, List<Items> items, float cost, Date time) throws IllegalArgumentException{
		super();
		this.name = name;
		this.items = items;
		if(cost<0) {
			throw new IllegalArgumentException ("Exception Occured cost cannot be less than zero ");
			
		}
		this.cost = cost;
		this.time = time;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Items> getItems() {
		return items;
	}
	public void setItems(List<Items> items) {
		this.items = items;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	@Override
	public String toString() {
		return "Bills [name=" + name + ", items=" + items + ", cost=" + cost + ", time=" + time + "]";
	}
	

}
