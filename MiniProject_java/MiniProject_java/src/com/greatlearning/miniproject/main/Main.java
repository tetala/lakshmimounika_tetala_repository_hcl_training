package com.greatlearning.miniproject.main;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.time.format.DateTimeFormatter;


import com.greatlearning.miniproject.Bills;
import com.greatlearning.miniproject.Items;
public class Main {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		Date time=new Date();
		List<Bills>bills=new ArrayList<>();
		List<Items>items=new ArrayList<>();
		Items i1=new Items(1,"Rajma chawal",2,150.0f);
		Items i2=new Items(2,"Mamos",2,190.0f);
		Items i3=new Items(3,"Red Thai Curry",2,180);
		Items i4=new Items(4,"chapp",2,190);
		Items i5=new Items(5,"chilli potatao",1,250);
		items.add(i1);
		items.add(i2);
		items.add(i3);
		items.add(i4);
		items.add(i5);
		System.out.println("------------------welcome to surabhi restaurant--------------------");
		while(true) {
			System.out.println("please enter the credencials....");
			System.out.println("enter your EMAIL address ");
			String email=sc.nextLine();
			System.out.println("enter a valid PASSWORD");
			String password=sc.nextLine();
			String name=Character.toUpperCase(password.charAt(0))+password.substring(1);
			System.out.println("enter A :if you are a admin\n"+"U: user\n"+"L to logout");
			String AorU=sc.nextLine();
			Bills bill=new Bills();
			List<Items>selectedItems=new ArrayList<>();
			float totalCost=0;
			 
			ZonedDateTime time1=ZonedDateTime.now();
			DateTimeFormatter f=DateTimeFormatter.ofPattern("E MMM dd HH:mm:ss zzz yyyy");
			String currentTime=time1.format(f);
			if(AorU.equals("U")|| AorU.equals("u")) {
				System.out.println("Welcome "+name);
				boolean finalOrder;
				do {
					System.out.println("Today's Items are:");
					items.stream().forEach(i->System.out.println(i));
					System.out.println("Enter the menu item code");
					int code=sc.nextInt();
					if(code==1) {
						selectedItems.add(i1);
						totalCost +=i1.getItemPrice();	
					}else if(code==2) {
						selectedItems.add(i2);
						totalCost +=i2.getItemPrice();
						
					}else if(code==3) {
						selectedItems.add(i3);
						totalCost +=i3.getItemPrice();
						
					}else if(code==4) {
						selectedItems.add(i4);
						totalCost +=i4.getItemPrice();
						
					}else {
						selectedItems.add(i5);
						totalCost +=i5.getItemPrice();		
					}
					System.out.println("Press 0 to show bill\n"+"press 1 to order more");
					int opt=sc.nextInt();
					if(opt==0)
						finalOrder=false;
					else
						finalOrder=true;
				}while(finalOrder);
				System.out.println("thank you"+name+"visit again");
				System.out.println("Items you have selected");
				selectedItems.stream().forEach(e->System.out.println(e));
				System.out.println("your total bill is"+totalCost);
				
				bill.setName(name);
				bill.setCost(totalCost);
				bill.setItems(selectedItems);
				bill.setTime(time);
				bills.add(bill);	
			}else if(AorU.equals("A")||AorU.equals("a")) {
				System.out.println("Welcome Admin");
				System.out.println("Press 1.To see all the bills\n"
						+ "2.To see total monthly bills"
						+ "3.To see total bills");
				int option=sc.nextInt();
				switch(option) {
				case 1:         
					           if(!bills.isEmpty()) {
					        	   for(Bills b: bills) {
					        		   if(b.getTime().getDate()==time.getDate()) {
											System.out.println("\nUsername :- " + b.getName());
											System.out.println("Items :- " + b.getItems());
											System.out.println("Total :- " + b.getCost());
											System.out.println("Date " + b.getTime() + "\n");
										}
					        	            
				              }
				              }else
				            	  
					              System.out.println("No! bills Today.");
				                    break;

				case 2:
					if (!bills.isEmpty()) {
						for (Bills b : bills) {
							if (b.getTime().getMonth()==time.getMonth()) {
							System.out.println("\nUsername :- " + b.getName());
							System.out.println("Items :- " + b.getItems());
							System.out.println("Total :- " + b.getCost());
							System.out.println("Date " + b.getTime() + "\n");
						}
						}
					} else
						System.out.println("No Bills for this month.!");
					break;

				case 3:
					if (!bills.isEmpty()) {
						for (Bills b : bills) {
							System.out.println("\nUsername :- " + b.getName());
							System.out.println("Items :- " + b.getItems());
							System.out.println("Total :- " + b.getCost());
							System.out.println("Date " + b.getTime() + "\n");
						}
					} else
						System.out.println("No Bills.!");

					break;

				default:
					System.out.println("Invalid Option");
					System.exit(1);
				}
			} else if (AorU.equals("L") || AorU.equals("l")) {
				System.exit(1);
				
			}else  {
				System.out.println("Invalid Entry");
			}                   
				
			      
			
			
			
			
		}
		
		
		

}
	}

