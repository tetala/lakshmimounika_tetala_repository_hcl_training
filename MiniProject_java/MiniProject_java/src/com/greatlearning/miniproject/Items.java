package com.greatlearning.miniproject;

public class Items {
	private int itemId;
	private String itemName;
	private int itemQuantity;
	private float itemPrice;
	
	public Items() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Items(int itemId, String itemName, int itemQuantity, float itemPrice) throws IllegalArgumentException  {
		super();
		if(itemId<0) {
			throw new IllegalArgumentException ("Exception ocurred Id cannot be less than zero");
		}
		this.itemId = itemId;
		if(itemName==null) {
			throw new IllegalArgumentException ("Exception ocurred itemName cannot be null");	
		}
		this.itemName = itemName;
		if(itemQuantity<0) {
			throw new IllegalArgumentException ("Exception ocurred itemQuantity cannot be less than zero");	
		}
		this.itemQuantity = itemQuantity;
        if(itemPrice<0) {
        	throw new IllegalArgumentException ("Exception ocurred price cannot be less than zero");
			
		}
		this.itemPrice = itemPrice;
		
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public float getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(float itemPrice) {
		this.itemPrice = itemPrice;
	}

	@Override
	public String toString() {
		return "Items [itemId=" + itemId + ", itemName=" + itemName + ", itemQuantity=" + itemQuantity + ", itemPrice="
				+ itemPrice + "]";
	}
	

}
