package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.LikedBooks;
import com.dao.LikedBooksDao;

@Service
public class LikedBooksService {
	
	@Autowired
	LikedBooksDao likedBooksDao;
	
	public String storeLikedBooks(LikedBooks likedBooks) {
		if(likedBooksDao.existsById(likedBooks.getLikedBooksCK())) {
			return "you already like this book";
		}else {
			likedBooksDao.save(likedBooks);
			return "Books Stored Sucessfully";
		}
		
	}
	
	public List<LikedBooks> getAllBooks(){
		return likedBooksDao.findAll();
	}
	

}
