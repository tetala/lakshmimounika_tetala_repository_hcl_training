package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.LikedBooks;
import com.service.LikedBooksService;

@RestController
@RequestMapping(value="likedBooks")
public class LikedBooksController {
	
	@Autowired
	LikedBooksService likedBooksService;
	
	@PostMapping(value="store")
	public String storeLikedBooks(@RequestBody LikedBooks likedBooks) {
		return likedBooksService.storeLikedBooks(likedBooks);
	}
	
	@GetMapping(value="get")
	public List<LikedBooks> getListOfAvailable(){
		return likedBooksService.getAllBooks();
	}
	
	
	
	 
	
}

