package com.bean;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class LikedBooksCK implements Serializable {
	private static final long serialVersionUID=1L;
	private int id;
	private String user;
	
	public LikedBooksCK() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public LikedBooksCK(int id, String user) {
		super();
		this.id = id;
		this.user = user;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}

}
