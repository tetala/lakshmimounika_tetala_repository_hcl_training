create database lakshmi_mounika_week9;

use lakshmi_mounika_week9;

create table admin(email varchar(20) primary key,password varchar(20));

create table users(email varchar(20) primary key,password varchar(20));

create table books(id int primary key,title varchar(50),author varchar(50),price float);

create table liked_books(sno int primary key auto_increment,id int ,title varchar(50),author varchar(50),price float,user varchar(20));
